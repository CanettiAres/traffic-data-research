import xml.etree.ElementTree as ET


class Elements:
    def __init__(self, element, type, info):
        self.type = type
        self.element = element
        self.info = info
        self.name = ""
        self.isRoad = False
        self.nodes = set()

    def set_street_name(self, streetname):
        self.name = streetname

    def add_node(self, node):
        self.nodes.add(node)


def xml_parsing(file):
    temp = []
    xmldoc = ET.parse(file)
    root = xmldoc.getroot()
    for child in root:
        element = child
        tag = child.tag
        attribs = child.attrib
        info = ["INFO"]
        for step_child in child:
            info.append(str(step_child.tag) + " " + str(step_child.attrib))
        new_entry = Elements(element, tag, info)
        temp.append(new_entry)
    return temp


def get_names(all_elements):
    for elem in all_elements:
        for inf in elem.info:
            if "tag" in inf and "name" in inf and "building" not in inf:
                parts = (inf.split("'v': '", 1))
                if len(parts) > 1:
                    street = parts[1].split("'}", 1)
                    if "tag" not in street[0] and elem.name == "":
                        elem.set_street_name(street[0])


def get_nodes(all_Elements):
    # Takes the info lines that contain info about nodes for the element and seperates the line to take only
    # the information we care about(the node number)
    for elem in all_Elements:
        for inf in elem.info:
            if "nd" in inf and "ref" in inf:
                parts = inf.split("'")
                elem.add_node(parts[3])


def remove_buildings(road):
    is_building = False
    for inf in road.info:
        if "building" in inf:
            is_building = True
        if "addr" in inf:
            is_building = True
        if "amenity" in inf:
            is_building = True
        if "route" in inf:
            is_building = True
        if "monument" in inf:
            is_building = True
        if "museum" in inf:
            is_building = True
        if "office" in inf:
            is_building = True
        if "tourism" in inf:
            is_building = True
        if "shop" in inf:
            is_building = True
        if "railway" in inf:
            is_building = True
        if "landuse" in inf:
            is_building = True
        if len(road.nodes) == 0:
            is_building = True
    if is_building is False:
        return road
    else:
        return None


def main():
    # Takes the given OSM data and treats it as an XML file in order to read in data and then creates objects
    # of the element type for the rest of the program to work with
    roads = xml_parsing('LilAustin.osm')
    get_names(roads)
    get_nodes(roads)
    new_roads = []
    for road in roads:
        is_road = remove_buildings(road)
        if is_road is not None:
            new_roads.append(is_road)
    for road in new_roads:
        if road.name is not "":
            print(road.name)


if __name__ == "__main__":
    main()
